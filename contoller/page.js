class Mainpage {

    //VIEW
          
    static mainPage (req, res, next) {
      res.render('main-page')
    }
    static suitGame (req, res, next) {
        res.render('play-game')
    }

    static loginPage (req, res, next) {
        res.render('login')
    }


    //MODELS
    static getPage(req, res, next) {
        try {
          res.send("menggunakan METHOD GET untuk PAGE")
    
        } catch (error) {
          next(error)
        }
      }
    
      static editPage(req, res, next) {
        try {
          res.send("menggunakan METHOD PUT untuk PAGE")
    
        } catch (error) {
          next(error)
        }
    
      }
    
      static deletePage(req, res, next) {
        try {
          res.send("menggunakan METHOD DELETE untuk PAGE")

        } catch (error) {
          next(error)
        }
    
      }
    
      static postPage(req, res, next) {
        try {
          res.send("menggunakan METHOD POST untuk PAGE")

        } catch (error) {
          next(error)
        }
      }
}


module.exports = Mainpage