const UserModel = require("../model/user.js")

class User {

  static getUser(req, res, next) {
    try {
      const _user = UserModel.getAllUser()

      res.status(200).json(_user)
    } catch (error) {
      next(error)
    }
  }

  static getUserByID(req, res, next) {
    try {
        const { id } = req.params
        const user = UserModel.getUserByID(id)
        res.status(200).json(user)
    } catch (error) {
        next(error)
    }
  }


  static login(req, res, next) {
    try {
      const { password, name } = req.body
      const user = UserModel.getAllUser()

      let idx = user.findIndex(x => x.name === name);
      
      const checkPassword = password == user[idx].password
      if (checkPassword == false) {
        throw {
          status: 401,
          message: "Unauthorize Access"
        }
      } else {
        res.render('play-game')
      }
    } catch (error) {
      console.log(error)
      next(error)
    }
  }
}

module.exports = User